1. N. Grossman et al., “Noninvasive Deep Brain Stimulation via Temporally Interfering Electric Fields,” Cell, vol. 169, no. 6, Art. no. 6, Jun. 2017, doi: 10.1016/j.cell.2017.05.024.
2. S. Rampersad et al., “Prospects for transcranial temporal interference stimulation in humans: A computational study,” NeuroImage, vol. 202, p. 116124, Nov. 2019, doi: 10.1016/j.neuroimage.2019.116124.
3. E. G. Lee et al., “Investigational Effect of Brain-Scalp Distance on the Efficacy of Transcranial Magnetic Stimulation Treatment in Depression,” IEEE Transactions on Magnetics, vol. 52, no. 7, Art. no. 7, Jul. 2016, doi: 10.1109/tmag.2015.2514158.
4. R. L. H. Erik G. Lee, “Population Head Model Repository.” IT’IS Foundation, 2016, doi: 10.13099/VIP-PHM-V1.0.
5. B. C. Hasgall PA Di Gennaro F, ““IT’IS Database for thermal and electromagnetic parameters of biological tissues”.” May 2018, doi: 10.13099/VIP21000-04-0.
6. P. Giacometti, K. L. Perdue, and S. G. Diamond, “Algorithm to find high density EEG scalp coordinates and analysis of their correspondence to structural and functional regions of the brain,” Journal of Neuroscience Methods, vol. 229, pp. 84–96, May 2014, doi: 10.1016/j.jneumeth.2014.04.020.
7. P. Giacometti, K. L. Perdue, and S. G. Diamond, “Mesh2EEG.” [Online]. Available: https://engineering.dartmouth.edu/multimodal/mesh2eeg.html.
8. Q. Zhou, “PyMesh.” GitHub, 2020, [Online]. Available: https://github.com/PyMesh/PyMesh.
9. H. Si, “TetGen, a Delaunay-Based Quality Tetrahedral Mesh Generator,” ACM Trans. Math. Softw., vol. 41, no. 2, Art. no. 2, Feb. 2015, doi: 10.1145/2629697.
10. R. Cimrman, V. Lukeš, and E. Rohan, “Multiscale finite element calculations in Python using SfePy,” Advances in Computational Mathematics, vol. 45, no. 4, Art. no. 4, May 2019, doi: 10.1007/s10444-019-09666-0.
11. D. Stoupis, “tACS Temporal Interference.” GitLab, 2020-11-28, [Online]. Available: https://gitlab.com/ttis-simulations.

